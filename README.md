![YouTube загрузчик видео на HTMX](https://gitflic.ru/project/zhirnoov/flowtube/blob/raw?file=flowtube-habr.png&commit=722da1e8819b216330c07851cf4c30035779001c)
[https://flowtube.ru/](https://flowtube.ru/)

**FlowTube** - удобный сервис для быстрого скачивания видео и аудио из YouTube по ссылке. 
Наслаждайтесь вашими любимыми контентом в любое время и в любом месте!"




## Подключение прокси для обхода ограничений от YouTube

Чтобы избежать ошибки *"Get the best YouTube experience. Sign in to save videos, subscribe, and more"*, если вы запустите сервис на облаке.

[Подробная документация по прокси от Ktor](https://ktor.io/docs/client-proxy.html)

Потребуется раскомментировать код в [InnerTubeAPI](https://gitflic.ru/project/zhirnoov/flowtube/blob?file=src/main/kotlin/ru/flowtube/innertube/InnerTubeAPI.kt&branch=master) и внести изменения:

    fun create() : InnerTubeApiImpl {  
      return InnerTubeApiImpl(client = HttpClient(CIO)   {  
      
      
      engine { 
         // Меняем на свой адрес прокси 
         proxy = ProxyBuilder.http("http://0.0.0.0:8000/")  
      }  
      
      defaultRequest { 
            //Авторируемся через Username и password
            val credentials = Base64.getEncoder().encodeToString("USERNAME:PASSWORD".toByteArray())  
            header(HttpHeaders.ProxyAuthorization, "Basic $credentials")  
         }  
      })