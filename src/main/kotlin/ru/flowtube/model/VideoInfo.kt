package ru.flowtube.model

import kotlinx.serialization.Serializable
import ru.flowtube.innertube.model.response.Formats

@Serializable
data class VideoInfo(
    val title : String = "",
    val shortDescription : String = "",
    val channelName : String = "",
    val thumbnail : String = "",
    val duration : String = "",
    val channelId : String  = "",
    val quality : List<Formats>
)