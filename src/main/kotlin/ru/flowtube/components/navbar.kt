package ru.flowtube.components

import kotlinx.html.*

fun FlowContent.navbar() = nav {
    classes = setOf("bg-white", " border-gray-200", "dark:bg-gray-900")

    div {
        classes = setOf("max-w-screen-xl", "flex flex-wrap", "items-center", "justify-between", "mx-auto", "p-4")

        a {
            classes = setOf("flex", "items-center", "space-x-3", "rtl:space-x-reverse")
            href = "https://flowtube.ru/"
            img {
                classes = setOf("h-10")
                src = "https://flowtube.ru/flowtube.svg"
                alt = "FlowTube Logo"
            }
            span {
                classes = setOf("self-center text-2xl font-semibold whitespace-nowrap dark:text-white")
                +"FlowTube"
            }
        }


        div {
            classes = setOf("flex", "md:order-2", "space-x-3", "md:space-x-0", "rtl:space-x-reverse")


            a {
            href = "https://apps.rustore.ru/app/ru.zhirnoov.youtubedownloader"
                button {
                    classes = setOf(
                        "text-white",
                        "bg-blue-700",
                        "hover:bg-blue-800",
                        "focus:ring-4",
                        "focus:outline-none",
                        "focus:ring-blue-300",
                        "font-medium",
                        "rounded-lg",
                        "text-sm",
                        "px-4",
                        "py-2",
                        "text-center",
                        "dark:bg-blue-600",
                        "dark:hover:bg-blue-700",
                        "dark:focus:ring-blue-800"
                    )
                    type = ButtonType.button
                    +"Установить"
                }


            }


            button {
                attributes["data-collapse-toggle"] = "navbar-cta"
                type = ButtonType.button
                classes = setOf(
                    "inline-flex",
                    "items-center",
                    "p-2",
                    "w-10",
                    "h-10",
                    "justify-center",
                    "text-sm",
                    "text-gray-500",
                    "rounded-lg",
                    "md:hidden",
                    "hover:bg-gray-100",
                    "focus:outline-none",
                    "focus:ring-2",
                    "focus:ring-gray-200",
                    "dark:text-gray-400",
                    "dark:hover:bg-gray-700",
                    "dark:focus:ring-gray-600"
                )
                attributes["aria-controls"] = "navbar-cta"
                attributes["aria-expanded"] = "false"

                span {
                    classes = setOf("sr-only")
                    +"Open main menu"
                }

                unsafe {
                    raw(
                        """
                        <svg class="w-5 h-5" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 17 14">
                        <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M1 1h15M1 7h15M1 13h15"/>
                    </svg>
                    """.trimIndent()
                    )

                }

            }

        }

        div {
            id = "navbar-cta"
            classes = setOf("items-center", "justify-between", "hidden", "w-full", "md:flex", "md:w-auto", "md:order-1")

            ul {

                classes = setOf(
                    "flex",
                    "flex-col",
                    "font-medium",
                    "p-4",
                    "md:p-0",
                    "mt-4",
                    "border",
                    "border-gray-100",
                    "rounded-lg",
                    "bg-gray-50",
                    "md:space-x-8",
                    "rtl:space-x-reverse",
                    "md:flex-row",
                    "md:mt-0",
                    "md:border-0",
                    "md:bg-white",
                    "dark:bg-gray-800",
                    "md:dark:bg-gray-900",
                    "dark:border-gray-700"
                )

                li {
                    a {
                        href = "/"
                        attributes["hx-push-url"] = "/"
                        classes = setOf(
                            "block",
                            "py-2",
                            "px-3",
                            "md:p-0",
                            "text-gray-900",
                            "rounded",
                            "hover:bg-gray-100",
                            "md:hover:bg-transparent",
                            "md:hover:text-blue-700",
                            "md:dark:hover:text-blue-500",
                            "dark:text-white",
                            "dark:hover:bg-gray-700",
                            "dark:hover:text-white",
                            "md:dark:hover:bg-transparent",
                            "dark:border-gray-700"
                        )
                        +"Главная"
                    }
                }

                li {
                    a {
                        href = "/videos"
                        attributes["hx-push-url"] = "/videos"

                        classes = setOf(
                            "block",
                            "py-2",
                            "px-3",
                            "md:p-0",
                            "text-gray-900",
                            "rounded",
                            "hover:bg-gray-100",
                            "md:hover:bg-transparent",
                            "md:hover:text-blue-700",
                            "md:dark:hover:text-blue-500",
                            "dark:text-white",
                            "dark:hover:bg-gray-700",
                            "dark:hover:text-white",
                            "md:dark:hover:bg-transparent",
                            "dark:border-gray-700"
                        )
                        +"Другие видео"
                    }
                }

                li {
                    a {
                        href = "/contacts"
                        /*attributes["hx-get"] = "/contacts-content"
                        attributes["hx-target"] = "#content"*/
                        attributes["hx-push-url"] = "/contacts"
                        classes = setOf(
                            "block",
                            "py-2",
                            "px-3",
                            "md:p-0",
                            "text-gray-900",
                            "rounded",
                            "hover:bg-gray-100",
                            "md:hover:bg-transparent",
                            "md:hover:text-blue-700",
                            "md:dark:hover:text-blue-500",
                            "dark:text-white",
                            "dark:hover:bg-gray-700",
                            "dark:hover:text-white",
                            "md:dark:hover:bg-transparent",
                            "dark:border-gray-700"
                        )
                        href = "/contacts"
                        +"Контакты"
                    }
                }
            }
        }
    }

}