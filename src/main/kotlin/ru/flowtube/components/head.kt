package ru.flowtube.components

import kotlinx.html.*

fun HTML.head(title : String, description : String) = head {
    meta {
        charset = "utf-8"
    }

    meta {
        name = "viewport"; content = "width=device-width, initial-scale=1.0"
    }

    meta {
        name = "robots"
        content = "index, follow"
    }

    meta {
        name = "description"
        content = description
    }


    meta {
        name="yandex-verification"
        content="36d086d9ce57c60b"
    }

    unsafe {
        raw("""
            <script>window.yaContextCb=window.yaContextCb||[]</script>
            <script src="https://yandex.ru/ads/system/context.js" async></script>
        """.trimIndent())
    }

    link {
        rel = "apple-touch-icon"
        sizes = "180x180"
        href = "/apple-touch-icon.png"
    }
    link {
        rel = "icon"
        type = "image/png"
        sizes = "32x32"
        href = "/favicon-32x32.png"
    }
    link {
        rel = "icon"
        type = "image/png"
        sizes = "16x16"
        href = "/favicon-16x16.png"
    }
    link {
        rel = "manifest"
        href = "/site.webmanifest"
    }
    link {
        rel = "mask-icon"
        href = "/safari-pinned-tab.svg"
        attributes["color"] = "#5bbad5"
    }
    meta {
        name = "msapplication-TileColor"
        content = "#b91d47"
    }
    meta {
        name = "theme-color"
        content = "#ffffff"
    }

    title {
        +title
    }

    unsafe {
        raw("""
            <script type="text/javascript" >
               (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
               m[i].l=1*new Date();
               for (var j = 0; j < document.scripts.length; j++) {if (document.scripts[j].src === r) { return; }}
               k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
               (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

               ym(97664011, "init", {
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
               });
            </script>
            <noscript><div><img src="https://mc.yandex.ru/watch/97664011" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
            <!-- /Yandex.Metrika counter -->
        """.trimIndent())
    }

    script { src = "https://cdn.tailwindcss.com" }
    script { src = "https://unpkg.com/htmx.org@2.0.0" }
    script {
        src = "https://cdnjs.cloudflare.com/ajax/libs/flowbite/2.3.0/flowbite.min.js"
    }
}