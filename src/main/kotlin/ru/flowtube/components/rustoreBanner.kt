package ru.flowtube.components

import kotlinx.html.*

fun FlowContent.rustoreBanner() = div {
    iframe {
        title = "Наши приложения в RuStore"
        classes = setOf(
            "pt-4",
            "px-4",
            "w-full",
            "mx-auto")
        height = "460"
        src =
            "https://www.rustore.ru/external/simple-selection?title=Наши%20приложения%20в%20RuStore&apps=ru.zhirnoov.youtubedownloader,ru.zhirnov.youtubeinbackground"
    }
}