package ru.flowtube

import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.engine.*
import io.ktor.server.html.*
import io.ktor.server.http.content.*
import io.ktor.server.netty.*
import io.ktor.server.request.*
import io.ktor.server.routing.*
import kotlinx.html.*
import ru.flowtube.innertube.InnerTubeRepository
import ru.flowtube.pages.contacts.contracts
import ru.flowtube.components.head
import ru.flowtube.pages.home.searchForm
import ru.flowtube.components.navbar
import ru.flowtube.components.rustoreBanner
import ru.flowtube.pages.videos.videos
import ru.flowtube.plugins.configureMonitoring
import java.util.regex.Pattern
import kotlin.collections.forEach
import kotlin.collections.setOf

fun main() {
    embeddedServer(Netty, port = 8080, host = "0.0.0.0", module = Application::module)
        .start(wait = true)
}

fun Application.module() {
    configureMonitoring()
    routing {

        staticResources("/", "static") {
            preCompressed(CompressedFileType.GZIP)
        }

        get("/") {
            call.respondHtml(HttpStatusCode.OK) {
                lang = "ru-RU"
                head(
                    title = "Загрузчик видео и аудио с YouTube",
                    description = "FlowTube - удобный сервис для быстрого скачивания видео и аудио из YouTube по ссылке. Наслаждайтесь вашими любимыми контентом в любое время и в любом месте!"
                )
                body {
                    classes = setOf("bg-white dark:bg-white")
                    navbar()
                    searchForm()
                    div {
                        id = "search-content"
                    }
                    rustoreBanner()
                }
            }
        }

        post("/searchvideo") {

            val videoUrl = call.receiveParameters()["url"].toString()

            val videoInfo = InnerTubeRepository().getVideoInfo(videoId = exactYouTubeId(videoUrl))

            call.respondHtml {
                body {
                    div {
                        classes = setOf(
                            "sm:w-full",
                            "md:w-full",
                            "lg:w-3/4",
                            "mx-auto",
                            "max-w-screen-lg",
                            "min-w-0",
                            "sm:block",
                            "md:flex",
                            "md:items-start",
                            "bg-gray-200"
                        )

                        div {
                            img {
                                //"h-1/4"
                                classes = setOf(
                                    "sm:mx-auto",
                                    "px-4",
                                    "md:ps-0",
                                    "max-w-80"
                                )
                                src = videoInfo.thumbnail
                                alt = videoInfo.title
                            }
                        }
                        div {
                            classes = setOf("sm:mx-auto", "md:w-1/2", "px-4")

                            h2 {
                                classes = setOf("font-medium", "text-xl", "mb-2")
                                +videoInfo.title
                            }

                            p {
                                classes = setOf("text-gray-700", "text-base")
                                +"Канал: ${videoInfo.channelName}"
                            }

                            p {
                                classes = setOf("text-gray-700", "text-base", "mt-2")
                                +"Длительность: ${secondsToTimeString(videoInfo.duration)}"
                            }


                            div {
                                classes = setOf("mt-4", "overflow-x-auto")
                                table {
                                    classes = setOf("w-full", "text-sm", "text-left", "text-gray-500")
                                    thead {
                                        tr {
                                            classes =
                                                setOf("text-xs", "text-gray-700", "uppercase", "bg-gray-50")
                                            th { +"Качество" }
                                            th { +"Размер" }
                                            // th { +"Download" }
                                        }
                                    }

                                    tbody {
                                        videoInfo.quality.forEach {
                                            tr {
                                                td {
                                                    classes = setOf("font-medium", "pt-4", "pb-4")
                                                    +if (it.qualityLabel == "") "Аудио (${
                                                        String.format(
                                                            "%.2f",
                                                            it.averageBitrate.toDouble() / 1000.0
                                                        )
                                                    } kbps)" else it.qualityLabel
                                                }


                                                td {
                                                    classes = setOf("font-medium")
                                                    +if (it.contentLength == "") "" else bytesToMegabytes(
                                                        filesize = it.contentLength.toLong()
                                                    )
                                                }
                                                td {

                                                    a {
                                                        target = "_blank"
                                                        rel = "noopener noreferrer"
                                                        href = it.url
                                                        button {
                                                            classes = setOf(
                                                                "text-white",
                                                                "bg-blue-700",
                                                                "hover:bg-blue-800",
                                                                "focus:ring-4",
                                                                "focus:outline-none",
                                                                "focus:ring-blue-300",
                                                                "font-medium",
                                                                "rounded-lg",
                                                                "text-sm",
                                                                "px-4",
                                                                "py-2",
                                                                "dark:bg-blue-600",
                                                                "dark:hover:bg-blue-700",
                                                                "dark:focus:ring-blue-800"
                                                            )
                                                            +"Скачать"
                                                        }
                                                    }


                                                }
                                            }
                                        }
                                    }


                                }
                            }
                        }
                    }
                }
            }
        }

        get("/contacts") {
            call.respondHtml {
                lang = "ru-RU"

                head(title = "Контакты | FlowTube", description = "Контакты для связи с разработчиками FlowTube")

                body {
                    classes = setOf("bg-white dark:bg-white")
                    navbar()
                    contracts()
                    rustoreBanner()
                }
            }
        }

        get("/videos") {
            call.respondHtml {
                lang = "ru-RU"
                head(title = "Другие видео | FlowTube", description = "Другие видео из FlowTube")

                body {
                    classes = setOf("bg-white dark:bg-white")
                    navbar()
                    videos()
                    rustoreBanner()
                }
            }
        }

    }


}

fun bytesToMegabytes(filesize: Long): String {
    val kiloByte = 1024L
    val megaByte = kiloByte * kiloByte
    val gigaByte = megaByte * kiloByte
    return if (filesize == 0L) {
        ""
    } else {
        if (filesize < gigaByte) {
            val megabytes = filesize.toDouble() / megaByte
            "${String.format("%.1f", megabytes)} MB"
        } else {
            val gigabytes = filesize.toDouble() / gigaByte
            "${String.format("%.1f", gigabytes)} GB"
        }
    }
}

fun secondsToTimeString(secondsString: String): String {
    val seconds = secondsString.toLongOrNull() ?: 0L
    val hours = seconds / 3600
    val minutes = (seconds % 3600) / 60
    val remainingSeconds = seconds % 60

    return when {
        hours > 0 -> String.format("%d:%02d:%02d", hours, minutes, remainingSeconds)
        minutes > 0 -> String.format("%d:%02d", minutes, remainingSeconds)
        else -> String.format("%d:%02d", minutes, remainingSeconds)
    }
}

fun exactYouTubeId(videoUrl: String): String {
    val regex = "(?<=v=|/videos/|embed\\/|shorts\\/|live\\/|music\\/|youtu.be\\/)[^#\\&\\?]*"
    val pattern = Pattern.compile(regex)
    val matcher = pattern.matcher(videoUrl)
    return if (matcher.find()) {
        matcher.group()
    } else {
        "0000"
    }
}