package ru.flowtube.innertube.model.body

import kotlinx.serialization.Serializable

@Serializable
data class Client(
    val androidSdkVersion: Int,
    val clientName: String,
    val clientScreen: String,
    val clientVersion: String,
    val gl: String,
    val hl: String
)