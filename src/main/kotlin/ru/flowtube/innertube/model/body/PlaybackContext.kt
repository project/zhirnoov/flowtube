package ru.flowtube.innertube.model.body

import kotlinx.serialization.Serializable

@Serializable
data class PlaybackContext(
    val contentPlaybackContext: ContentPlaybackContext
)