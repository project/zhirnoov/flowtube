package ru.flowtube.innertube.model.body

import kotlinx.serialization.Serializable

@Serializable
data class ContentPlaybackContext(
    val signatureTimestamp: Int
)