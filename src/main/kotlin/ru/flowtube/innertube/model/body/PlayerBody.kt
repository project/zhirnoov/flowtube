package ru.flowtube.innertube.model.body

import kotlinx.serialization.Serializable

@Serializable
data class PlayerBody(
    val contentCheckOk: Boolean,
    val context: Context,
    val playbackContext: PlaybackContext,
    val racyCheckOk: Boolean,
    val videoId: String
)