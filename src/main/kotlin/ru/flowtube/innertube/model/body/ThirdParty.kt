package ru.flowtube.innertube.model.body
import kotlinx.serialization.Serializable

@Serializable
data class ThirdParty(
    val embedUrl: String
)