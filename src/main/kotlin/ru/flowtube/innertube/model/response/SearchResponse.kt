package ru.flowtube.innertube.model.response

import kotlinx.serialization.Serializable

@Serializable
data class SearchResponse(val streamingData : StreamingData, val videoDetails : VideoDetails)