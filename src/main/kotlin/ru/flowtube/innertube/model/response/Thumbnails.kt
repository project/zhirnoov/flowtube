package ru.flowtube.innertube.model.response
import kotlinx.serialization.Serializable

@Serializable
data class Thumbnails(val url : String, val width : Long, val height : Long)