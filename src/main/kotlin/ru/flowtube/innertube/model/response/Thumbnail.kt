package ru.flowtube.innertube.model.response
import kotlinx.serialization.Serializable

@Serializable
data class Thumbnail(val thumbnails : List<Thumbnails>)
