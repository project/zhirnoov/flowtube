package ru.flowtube.innertube.model.response

import kotlinx.serialization.Serializable

@Serializable
data class StreamingData(
    val formats : List<Formats>,
    val adaptiveFormats : List<Formats>
)