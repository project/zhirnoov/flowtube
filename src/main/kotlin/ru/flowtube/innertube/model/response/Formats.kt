package ru.flowtube.innertube.model.response

import kotlinx.serialization.Serializable

@Serializable
data class Formats(val itag: Long,
                   val url: String,
                   val contentLength: String = "",
                   val qualityLabel : String = "",
                   val mimeType : String = "",
                   val averageBitrate : String = ""
)