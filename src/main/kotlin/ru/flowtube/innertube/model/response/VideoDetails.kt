package ru.flowtube.innertube.model.response
import kotlinx.serialization.Serializable

@Serializable
data class VideoDetails(
    val videoId : String,
    val title : String,
    val lengthSeconds: String,
    val thumbnail : Thumbnail,
    val shortDescription : String,
    val viewCount : String,
    val author : String
)