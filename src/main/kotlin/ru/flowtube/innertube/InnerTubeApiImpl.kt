package ru.flowtube.innertube

import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.request.*
import io.ktor.http.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import ru.flowtube.innertube.model.body.*
import ru.flowtube.innertube.model.response.SearchResponse
import ru.flowtube.model.VideoInfo

class InnerTubeApiImpl(private val client: HttpClient) : InnerTubeAPI {


    override suspend fun getVideoInfo(videoId: String): VideoInfo {
        return try {
            withContext(Dispatchers.IO) {

                val body = PlayerBody(
                    contentCheckOk = true, context = Context(
                        client = Client(
                            androidSdkVersion = 31,
                            clientName = "ANDROID_VR",
                            clientScreen = "WATCH",
                            clientVersion = "1.28.63",
                            gl = "US",
                            hl = "en"
                        ), thirdParty = ThirdParty(embedUrl = "https://www.youtube.com/")
                    ), playbackContext = PlaybackContext(
                        contentPlaybackContext = ContentPlaybackContext(
                            signatureTimestamp = 19250
                        )
                    ), racyCheckOk = true, videoId = videoId
                )

                val response = client.post("https://www.youtube.com/youtubei/v1/player") {
                    contentType(ContentType.Application.Json)
                    setBody(body)
                }
                val searchResponse: SearchResponse = response.body()
                return@withContext VideoInfo(
                    title = searchResponse.videoDetails.title,
                    channelName = searchResponse.videoDetails.author,
                    shortDescription = searchResponse.videoDetails.shortDescription,
                    duration = searchResponse.videoDetails.lengthSeconds,
                    thumbnail = searchResponse.videoDetails.thumbnail.thumbnails.last().url,
                    quality = searchResponse.streamingData.formats + searchResponse.streamingData.adaptiveFormats.filter { it.mimeType.contains("audio/webm") }
                )
            }

        } catch (e: Exception) {
            VideoInfo(title = "", channelName = "", thumbnail = "", duration = "", quality = emptyList())
        }
    }


}