package ru.flowtube.innertube

import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.plugins.logging.*
import io.ktor.serialization.kotlinx.json.json
import kotlinx.serialization.json.Json
import ru.flowtube.model.VideoInfo


interface InnerTubeAPI {

    suspend fun getVideoInfo(videoId : String) : VideoInfo

    companion object {

        fun create() : InnerTubeApiImpl {
          return InnerTubeApiImpl(client = HttpClient(CIO)   {


              /*engine {
                  proxy = ProxyBuilder.http("http://0.0.0.0:8000/")
              }

              defaultRequest {
                  val credentials = Base64.getEncoder().encodeToString("USERNAME:PASSWORD".toByteArray())
                  header(HttpHeaders.ProxyAuthorization, "Basic $credentials")
              }*/

              install(Logging) {
                  level = LogLevel.INFO
              }

              install(ContentNegotiation) {
                  json(
                      Json {
                          isLenient = true
                          ignoreUnknownKeys = true
                          allowSpecialFloatingPointValues = true
                          useArrayPolymorphism = true
                      }
                  )
              }
          })
        }
    }
}