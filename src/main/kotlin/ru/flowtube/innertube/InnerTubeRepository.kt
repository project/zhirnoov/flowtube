package ru.flowtube.innertube

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import ru.flowtube.model.VideoInfo

class InnerTubeRepository {

    suspend fun getVideoInfo(videoId: String): VideoInfo {
        return withContext(Dispatchers.IO) {
            try {
                val service = InnerTubeAPI.create()
                service.getVideoInfo(videoId)
            } catch (e: Exception) {
                VideoInfo(title = "", channelName = "", thumbnail = "", duration = "", quality = emptyList())
            }
        }
    }

}