package ru.flowtube.pages.contacts

import kotlinx.html.*

fun FlowContent.contracts() = div {
    header {
        classes = setOf("bg-white", "shadow")
        div {
            classes = setOf("max-w-7xl", "mx-auto", "p-4")
            h1 {
                classes = setOf("text-3xl", "font-bold", "tracking-tight", "text-gray-900")
                +"Контакты"
            }
        }
    }

    main {
        classes = setOf("max-w-7xl", "p-4")
        p {
            classes = setOf("text-2xl", "text-gray-900")
            +("По предложениям, жалобам, сотрудничеству и т.д")
        }
    }

    div {
        classes = setOf("p-4")
        p {
            classes = setOf("text-xl")
            +"Наш Telegram канал"
        }
        a {
            classes = setOf("pt-2")
            href = "https://t.me/startup_everyday"
            +"https://t.me/startup_everyday"
        }

        p {
            classes = setOf("pt-4", "text-xl")
            +"Эл. почта"
        }

        p {
            classes = setOf("pt-2")
            +"zhirnoov@yandex.ru"
        }
    }

}